#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: psk_usrp
# GNU Radio version: 3.9.4.0

from distutils.version import StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

import os
import sys
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from PyQt5 import Qt
from gnuradio import qtgui
from gnuradio.filter import firdes
import sip
from gnuradio import analog
from gnuradio import blocks
import pmt
from gnuradio import digital
from gnuradio import gr
from gnuradio.fft import window
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
import time
from gnuradio.qtgui import Range, RangeWidget
from PyQt5 import QtCore
from mpsk_receiver import mpsk_receiver  # grc-generated hier_block



from gnuradio import qtgui

class psk_usrp(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "psk_usrp", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("psk_usrp")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "psk_usrp")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 4e5
        self.timing_loop_bw = timing_loop_bw = 6.28/100.0
        self.sps = sps = 4
        self.qpsk = qpsk = digital.constellation_rect([0.707+0.707j, -0.707+0.707j, -0.707-0.707j, 0.707-0.707j], [0, 1, 2, 3],
        4, 2, 2, 1, 1).base()
        self.phase_bw = phase_bw = 6.28/100.0
        self.lp = lp = firdes.low_pass(1.0, samp_rate, 70e3,20e3, window.WIN_HAMMING, 6.76)
        self.interpolate = interpolate = 40
        self.gain = gain = 70
        self.freq = freq = 1.5e9
        self.excess_bw = excess_bw = 0.35
        self.eq_gain = eq_gain = 0.01
        self.decimation = decimation = 40
        self.abc = abc = digital.header_format_default('11100001010110101110100010010011',2, 1)

        ##################################################
        # Blocks
        ##################################################
        self.controls = Qt.QTabWidget()
        self.controls_widget_0 = Qt.QWidget()
        self.controls_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.controls_widget_0)
        self.controls_grid_layout_0 = Qt.QGridLayout()
        self.controls_layout_0.addLayout(self.controls_grid_layout_0)
        self.controls.addTab(self.controls_widget_0, 'Receiver')
        self.top_grid_layout.addWidget(self.controls, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._timing_loop_bw_range = Range(0, 0.2, 0.01, 6.28/100.0, 200)
        self._timing_loop_bw_win = RangeWidget(self._timing_loop_bw_range, self.set_timing_loop_bw, "Time: BW", "slider", float, QtCore.Qt.Horizontal)
        self.controls_grid_layout_0.addWidget(self._timing_loop_bw_win, 0, 0, 1, 1)
        for r in range(0, 1):
            self.controls_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 1):
            self.controls_grid_layout_0.setColumnStretch(c, 1)
        self._phase_bw_range = Range(0.0, 1.0, 0.01, 6.28/100.0, 200)
        self._phase_bw_win = RangeWidget(self._phase_bw_range, self.set_phase_bw, "Phase: Bandwidth", "slider", float, QtCore.Qt.Horizontal)
        self.controls_grid_layout_0.addWidget(self._phase_bw_win, 0, 2, 1, 1)
        for r in range(0, 1):
            self.controls_grid_layout_0.setRowStretch(r, 1)
        for c in range(2, 3):
            self.controls_grid_layout_0.setColumnStretch(c, 1)
        self.figures = Qt.QTabWidget()
        self.figures_widget_0 = Qt.QWidget()
        self.figures_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.figures_widget_0)
        self.figures_grid_layout_0 = Qt.QGridLayout()
        self.figures_layout_0.addLayout(self.figures_grid_layout_0)
        self.figures.addTab(self.figures_widget_0, 'Eye Diagram')
        self.figures_widget_1 = Qt.QWidget()
        self.figures_layout_1 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.figures_widget_1)
        self.figures_grid_layout_1 = Qt.QGridLayout()
        self.figures_layout_1.addLayout(self.figures_grid_layout_1)
        self.figures.addTab(self.figures_widget_1, 'Constellation')
        self.figures_widget_2 = Qt.QWidget()
        self.figures_layout_2 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.figures_widget_2)
        self.figures_grid_layout_2 = Qt.QGridLayout()
        self.figures_layout_2.addLayout(self.figures_grid_layout_2)
        self.figures.addTab(self.figures_widget_2, 'Frequency')
        self.figures_widget_3 = Qt.QWidget()
        self.figures_layout_3 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.figures_widget_3)
        self.figures_grid_layout_3 = Qt.QGridLayout()
        self.figures_layout_3.addLayout(self.figures_grid_layout_3)
        self.figures.addTab(self.figures_widget_3, 'Transmit Eye Diagram')
        self.top_grid_layout.addWidget(self.figures, 2, 0, 1, 1)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._eq_gain_range = Range(0.0, 0.1, 0.001, 0.01, 200)
        self._eq_gain_win = RangeWidget(self._eq_gain_range, self.set_eq_gain, "Equalizer: rate", "slider", float, QtCore.Qt.Horizontal)
        self.controls_grid_layout_0.addWidget(self._eq_gain_win, 0, 1, 1, 1)
        for r in range(0, 1):
            self.controls_grid_layout_0.setRowStretch(r, 1)
        for c in range(1, 2):
            self.controls_grid_layout_0.setColumnStretch(c, 1)
        self.uhd_usrp_source_0 = uhd.usrp_source(
            ",".join(("", '')),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
        )
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0.set_time_unknown_pps(uhd.time_spec(0))

        self.uhd_usrp_source_0.set_center_freq(freq, 0)
        self.uhd_usrp_source_0.set_antenna("RX2", 0)
        self.uhd_usrp_source_0.set_rx_agc(False, 0)
        self.uhd_usrp_source_0.set_gain(gain, 0)
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
            ",".join(("", '')),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
            "",
        )
        self.uhd_usrp_sink_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0.set_time_unknown_pps(uhd.time_spec(0))

        self.uhd_usrp_sink_0.set_center_freq(freq, 0)
        self.uhd_usrp_sink_0.set_antenna("TX/RX", 0)
        self.uhd_usrp_sink_0.set_gain(gain, 0)
        self.qtgui_eye_sink_x_0_0_0 = qtgui.eye_sink_c(
            1024, #size
            samp_rate/4/decimation, #samp_rate
            1, #number of inputs
            None
        )
        self.qtgui_eye_sink_x_0_0_0.set_update_time(0.10)
        self.qtgui_eye_sink_x_0_0_0.set_samp_per_symbol(1)
        self.qtgui_eye_sink_x_0_0_0.set_y_axis(-1, 1)

        self.qtgui_eye_sink_x_0_0_0.set_y_label('Receiver_Eye_Diagram', "")

        self.qtgui_eye_sink_x_0_0_0.enable_tags(True)
        self.qtgui_eye_sink_x_0_0_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_eye_sink_x_0_0_0.enable_autoscale(False)
        self.qtgui_eye_sink_x_0_0_0.enable_grid(True)
        self.qtgui_eye_sink_x_0_0_0.enable_axis_labels(True)
        self.qtgui_eye_sink_x_0_0_0.enable_control_panel(True)


        labels = ['Signal 1', 'Signal 2', 'Signal 3', 'Signal 4', 'Signal 5',
            'Signal 6', 'Signal 7', 'Signal 8', 'Signal 9', 'Signal 10']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'blue', 'blue', 'blue', 'blue',
            'blue', 'blue', 'blue', 'blue', 'blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(2):
            if len(labels[i]) == 0:
                if (i % 2 == 0):
                    self.qtgui_eye_sink_x_0_0_0.set_line_label(i, "Eye [Re{{Data {0}}}]".format(round(i/2)))
                else:
                    self.qtgui_eye_sink_x_0_0_0.set_line_label(i, "Eye [Im{{Data {0}}}]".format(round((i-1)/2)))
            else:
                self.qtgui_eye_sink_x_0_0_0.set_line_label(i, labels[i])
            self.qtgui_eye_sink_x_0_0_0.set_line_width(i, widths[i])
            self.qtgui_eye_sink_x_0_0_0.set_line_color(i, colors[i])
            self.qtgui_eye_sink_x_0_0_0.set_line_style(i, styles[i])
            self.qtgui_eye_sink_x_0_0_0.set_line_marker(i, markers[i])
            self.qtgui_eye_sink_x_0_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_eye_sink_x_0_0_0_win = sip.wrapinstance(self.qtgui_eye_sink_x_0_0_0.qwidget(), Qt.QWidget)
        self.figures_layout_0.addWidget(self._qtgui_eye_sink_x_0_0_0_win)
        self.mpsk_receiver_0 = mpsk_receiver(
            eq_gain=eq_gain,
            phase_bw=phase_bw,
            samp_rate=samp_rate,
            timing_loop_bw=timing_loop_bw,
        )

        self.top_layout.addWidget(self.mpsk_receiver_0)
        self.digital_protocol_parser_b_0 = digital.protocol_parser_b(abc)
        self.digital_protocol_formatter_bb_0 = digital.protocol_formatter_bb(abc, 'packet_len')
        self.digital_header_payload_demux_0 = digital.header_payload_demux(
            400,
            1,
            0,
            'packet_len',
            'packet_len',
            False,
            gr.sizeof_char,
            '',
            samp_rate/4,
            [],
            0)
        self.digital_constellation_modulator_0 = digital.generic_mod(
            constellation=qpsk,
            differential=True,
            samples_per_symbol=sps,
            pre_diff_code=True,
            excess_bw=excess_bw,
            verbose=False,
            log=False,
            truncate=False)
        self.blocks_unpack_k_bits_bb_0 = blocks.unpack_k_bits_bb(8)
        self.blocks_tagged_stream_mux_0 = blocks.tagged_stream_mux(gr.sizeof_char*1, 'packet_len', 0)
        self.blocks_stream_to_tagged_stream_0 = blocks.stream_to_tagged_stream(gr.sizeof_char, 1, 400, 'packet_len')
        self.blocks_null_sink_0_0 = blocks.null_sink(gr.sizeof_char*1)
        self.blocks_null_sink_0 = blocks.null_sink(gr.sizeof_gr_complex*1)
        self.blocks_float_to_char_0 = blocks.float_to_char(1, 1)
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_char*1, 'C:\\Users\\Administrator\\Downloads\\mersin.jpg', False, 0, 0)
        self.blocks_file_source_0.set_begin_tag(pmt.PMT_NIL)
        self.blocks_file_sink_2 = blocks.file_sink(gr.sizeof_char*1, 'C:\\Users\\Administrator\\Documents\\header_file', False)
        self.blocks_file_sink_2.set_unbuffered(False)
        self.blocks_file_sink_1_0 = blocks.file_sink(gr.sizeof_char*1, 'C:\\Users\\Administrator\\Documents\\in_bits', False)
        self.blocks_file_sink_1_0.set_unbuffered(False)
        self.blocks_file_sink_1 = blocks.file_sink(gr.sizeof_char*1, 'C:\\Users\\Administrator\\Documents\\in', False)
        self.blocks_file_sink_1.set_unbuffered(False)
        self.blocks_file_sink_0_1 = blocks.file_sink(gr.sizeof_char*1, 'C:\\Users\\Administrator\\Documents\\out', False)
        self.blocks_file_sink_0_1.set_unbuffered(False)
        self.blocks_file_sink_0_0 = blocks.file_sink(gr.sizeof_char*1, 'C:\\Users\\Administrator\\Documents\\out_bits', False)
        self.blocks_file_sink_0_0.set_unbuffered(False)
        self.analog_agc2_xx_0 = analog.agc2_cc(1e-1, 1e-2, 1.0, 1.0)
        self.analog_agc2_xx_0.set_max_gain(65536)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.digital_protocol_parser_b_0, 'info'), (self.digital_header_payload_demux_0, 'header_data'))
        self.connect((self.analog_agc2_xx_0, 0), (self.mpsk_receiver_0, 0))
        self.connect((self.blocks_file_source_0, 0), (self.blocks_file_sink_1, 0))
        self.connect((self.blocks_file_source_0, 0), (self.blocks_stream_to_tagged_stream_0, 0))
        self.connect((self.blocks_file_source_0, 0), (self.blocks_unpack_k_bits_bb_0, 0))
        self.connect((self.blocks_float_to_char_0, 0), (self.blocks_file_sink_0_0, 0))
        self.connect((self.blocks_float_to_char_0, 0), (self.digital_header_payload_demux_0, 0))
        self.connect((self.blocks_float_to_char_0, 0), (self.digital_header_payload_demux_0, 1))
        self.connect((self.blocks_float_to_char_0, 0), (self.digital_protocol_parser_b_0, 0))
        self.connect((self.blocks_stream_to_tagged_stream_0, 0), (self.blocks_tagged_stream_mux_0, 1))
        self.connect((self.blocks_stream_to_tagged_stream_0, 0), (self.digital_protocol_formatter_bb_0, 0))
        self.connect((self.blocks_tagged_stream_mux_0, 0), (self.blocks_file_sink_2, 0))
        self.connect((self.blocks_tagged_stream_mux_0, 0), (self.digital_constellation_modulator_0, 0))
        self.connect((self.blocks_unpack_k_bits_bb_0, 0), (self.blocks_file_sink_1_0, 0))
        self.connect((self.digital_constellation_modulator_0, 0), (self.uhd_usrp_sink_0, 0))
        self.connect((self.digital_header_payload_demux_0, 1), (self.blocks_file_sink_0_1, 0))
        self.connect((self.digital_header_payload_demux_0, 0), (self.blocks_null_sink_0_0, 0))
        self.connect((self.digital_protocol_formatter_bb_0, 0), (self.blocks_tagged_stream_mux_0, 0))
        self.connect((self.mpsk_receiver_0, 0), (self.blocks_float_to_char_0, 0))
        self.connect((self.mpsk_receiver_0, 1), (self.blocks_null_sink_0, 0))
        self.connect((self.mpsk_receiver_0, 1), (self.qtgui_eye_sink_x_0_0_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.analog_agc2_xx_0, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "psk_usrp")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_lp(firdes.low_pass(1.0, self.samp_rate, 70e3, 20e3, window.WIN_HAMMING, 6.76))
        self.mpsk_receiver_0.set_samp_rate(self.samp_rate)
        self.qtgui_eye_sink_x_0_0_0.set_samp_rate(self.samp_rate/4/self.decimation)
        self.uhd_usrp_sink_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)

    def get_timing_loop_bw(self):
        return self.timing_loop_bw

    def set_timing_loop_bw(self, timing_loop_bw):
        self.timing_loop_bw = timing_loop_bw
        self.mpsk_receiver_0.set_timing_loop_bw(self.timing_loop_bw)

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps

    def get_qpsk(self):
        return self.qpsk

    def set_qpsk(self, qpsk):
        self.qpsk = qpsk

    def get_phase_bw(self):
        return self.phase_bw

    def set_phase_bw(self, phase_bw):
        self.phase_bw = phase_bw
        self.mpsk_receiver_0.set_phase_bw(self.phase_bw)

    def get_lp(self):
        return self.lp

    def set_lp(self, lp):
        self.lp = lp

    def get_interpolate(self):
        return self.interpolate

    def set_interpolate(self, interpolate):
        self.interpolate = interpolate

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.uhd_usrp_sink_0.set_gain(self.gain, 0)
        self.uhd_usrp_source_0.set_gain(self.gain, 0)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.uhd_usrp_sink_0.set_center_freq(self.freq, 0)
        self.uhd_usrp_source_0.set_center_freq(self.freq, 0)

    def get_excess_bw(self):
        return self.excess_bw

    def set_excess_bw(self, excess_bw):
        self.excess_bw = excess_bw

    def get_eq_gain(self):
        return self.eq_gain

    def set_eq_gain(self, eq_gain):
        self.eq_gain = eq_gain
        self.mpsk_receiver_0.set_eq_gain(self.eq_gain)

    def get_decimation(self):
        return self.decimation

    def set_decimation(self, decimation):
        self.decimation = decimation
        self.qtgui_eye_sink_x_0_0_0.set_samp_rate(self.samp_rate/4/self.decimation)

    def get_abc(self):
        return self.abc

    def set_abc(self, abc):
        self.abc = abc




def main(top_block_cls=psk_usrp, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print("Error: failed to enable real-time scheduling.")

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
