"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!
"""

import numpy as np
from gnuradio import gr
import pmt


class blk(gr.sync_block):  # other base classes are basic_block, decim_block, interp_block
    """Remove packet tail"""

    def __init__(self):  # only default arguments here
        """arguments to this function show up as parameters in GRC"""
        gr.sync_block.__init__(
            self,
            name='RemoveTailK2Sat',   # will show up in GRC
            in_sig=[],
            out_sig=[]
        )
        self.message_port_register_in(pmt.intern('in'))
        self.set_msg_handler(pmt.intern('in'), self.handle_msg)
        self.message_port_register_out(pmt.intern('out'))

        # CRC-16 table construction
        self.crc_table = list()
        for i in range(256):
            tmp = 0
            if i & 1:
                tmp ^= 0x1021
            if i & 2:
                tmp ^= 0x2042
            if i & 4:
                tmp ^= 0x4084
            if i & 8:
                tmp ^= 0x8108
            if i & 16:
                tmp ^= 0x1231
            if i & 32:
                tmp ^= 0x2462
            if i & 64:
                tmp ^= 0x48C4
            if i & 128:
                tmp ^= 0x9188
            self.crc_table.append(tmp)

    def check_packet(self, packet):
        data = b'\x7e' + packet  # add 0x7e HDLC flag for CRC-16 check
        checksum = 0xFFFF
        for d in data:
            checksum = (((checksum << 8) & 0xFF00)
                        ^ self.crc_table[((checksum >> 8) ^ d) & 0x00FF])
        return checksum == 0

    def handle_msg(self, msg_pmt):
        # print('Received message')
        msg = pmt.cdr(msg_pmt)
        if not pmt.is_u8vector(msg):
            print('[ERROR] Received invalid message type. Expected u8vector')
            return
        packet = bytes(pmt.u8vector_elements(msg))

        # Search all packet end markers in current packet
        start = 0
        while True:
            # Find packet end marker
            idx = packet[start:].find(b'\x7e\x55\x55\x55')
            if idx == -1:
                break
            # Check if this is a valid packet
            if self.check_packet(packet[:idx]):
                ax25_packet = packet[:-2+idx]
                ax25_packet = list(ax25_packet)  # conversion to list for pybind11
                # print("Index: ",idx, "Length of packet: " , len(ax25_packet))
                self.message_port_pub(
                    pmt.intern('out'),
                    pmt.cons(pmt.PMT_NIL,
                             pmt.init_u8vector(len(ax25_packet), ax25_packet)))
            start = idx + 2

        return