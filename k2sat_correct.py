#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: K2SAT Corrected
# GNU Radio version: v3.10.0.0-17-gb42996b1

from gnuradio import blocks
import pmt
from gnuradio import digital
from gnuradio import fec
from gnuradio import filter
from gnuradio.filter import firdes
from gnuradio import gr
from gnuradio.fft import window
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import k2sat_correct_epy_block_0 as epy_block_0  # embedded python block
import satellites
import satellites.components.datasinks
import satellites.hier




class k2sat_correct(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "K2SAT Corrected", catch_exceptions=True)

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 2e6
        self.samp_per_sym = samp_per_sym = 4
        self.nfilts = nfilts = 16
        self.variable_1 = variable_1 = 0
        self.syncword = syncword = "0101010101111110"
        self.rrc_taps = rrc_taps = firdes.root_raised_cosine(nfilts, nfilts,1.0/float(samp_per_sym), 0.35, 11*samp_per_sym*nfilts)
        self.packet_len = packet_len = 2200
        self.lowpass_taps = lowpass_taps = firdes.low_pass(1.0, samp_rate, 4*100e3,4*10e3, window.WIN_HAMMING, 6.76)
        self.dec_cc_0 = dec_cc_0 = fec.cc_decoder.make(80,7, 2, [79,109], 0, -1, fec.CC_STREAMING, False)
        self.dec_cc = dec_cc = fec.cc_decoder.make(80,7, 2, [79,109], 0, -1, fec.CC_STREAMING, False)

        ##################################################
        # Blocks
        ##################################################
        self.satellites_sync_to_pdu_packed_0_0 = satellites.hier.sync_to_pdu_packed(
            packlen=packet_len,
            sync=syncword,
            threshold=0,
        )
        self.satellites_sync_to_pdu_packed_0 = satellites.hier.sync_to_pdu_packed(
            packlen=packet_len,
            sync=syncword,
            threshold=0,
        )
        self.satellites_rms_agc_0 = satellites.hier.rms_agc(alpha=1e-3, reference=0.5)
        self.satellites_k2sat_deframer_0 = satellites.k2sat_deframer()
        self.satellites_file_receiver_0 = satellites.components.datasinks.file_receiver('k2sat', '', False, options="")
        self.satellites_descrambler308_0_0 = satellites.descrambler308()
        self.satellites_descrambler308_0 = satellites.descrambler308()
        self.freq_xlating_fir_filter_xxx_0 = filter.freq_xlating_fir_filter_ccc(1, lowpass_taps, 500e3, samp_rate)
        self.fec_extended_decoder_0_0 = fec.extended_decoder(decoder_obj_list=dec_cc, threading= None, ann=None, puncpat='11', integration_period=10000)
        self.fec_extended_decoder_0 = fec.extended_decoder(decoder_obj_list=dec_cc_0, threading= None, ann=None, puncpat='11', integration_period=10000)
        self.epy_block_0 = epy_block_0.blk()
        self.digital_pfb_clock_sync_xxx_0 = digital.pfb_clock_sync_ccf(samp_per_sym, 5e-3, rrc_taps, 32, 16, 1e-3, 1)
        self.digital_diff_decoder_bb_0_0 = digital.diff_decoder_bb(2, digital.DIFF_DIFFERENTIAL)
        self.digital_diff_decoder_bb_0 = digital.diff_decoder_bb(2, digital.DIFF_DIFFERENTIAL)
        self.digital_costas_loop_cc_0 = digital.costas_loop_cc(0.010, 4, False)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_cc(1j)
        self.blocks_interleave_0_0 = blocks.interleave(gr.sizeof_float*1, 1)
        self.blocks_interleave_0 = blocks.interleave(gr.sizeof_float*1, 1)
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_gr_complex*1, '/home/pi/Downloads/samp_rate_2M_photo_data_finalpacket_2', False, 0, 0)
        self.blocks_file_source_0.set_begin_tag(pmt.PMT_NIL)
        self.blocks_complex_to_float_0_0 = blocks.complex_to_float(1)
        self.blocks_complex_to_float_0 = blocks.complex_to_float(1)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.epy_block_0, 'out'), (self.satellites_file_receiver_0, 'in'))
        self.msg_connect((self.satellites_k2sat_deframer_0, 'out'), (self.epy_block_0, 'in'))
        self.msg_connect((self.satellites_sync_to_pdu_packed_0, 'out'), (self.satellites_k2sat_deframer_0, 'in'))
        self.msg_connect((self.satellites_sync_to_pdu_packed_0_0, 'out'), (self.satellites_k2sat_deframer_0, 'in'))
        self.connect((self.blocks_complex_to_float_0, 1), (self.blocks_interleave_0, 1))
        self.connect((self.blocks_complex_to_float_0, 0), (self.blocks_interleave_0, 0))
        self.connect((self.blocks_complex_to_float_0_0, 1), (self.blocks_interleave_0_0, 1))
        self.connect((self.blocks_complex_to_float_0_0, 0), (self.blocks_interleave_0_0, 0))
        self.connect((self.blocks_file_source_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.blocks_interleave_0, 0), (self.fec_extended_decoder_0, 0))
        self.connect((self.blocks_interleave_0_0, 0), (self.fec_extended_decoder_0_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_complex_to_float_0_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.freq_xlating_fir_filter_xxx_0, 0))
        self.connect((self.digital_costas_loop_cc_0, 0), (self.digital_pfb_clock_sync_xxx_0, 0))
        self.connect((self.digital_diff_decoder_bb_0, 0), (self.satellites_descrambler308_0, 0))
        self.connect((self.digital_diff_decoder_bb_0_0, 0), (self.satellites_descrambler308_0_0, 0))
        self.connect((self.digital_pfb_clock_sync_xxx_0, 0), (self.blocks_complex_to_float_0, 0))
        self.connect((self.digital_pfb_clock_sync_xxx_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.fec_extended_decoder_0, 0), (self.digital_diff_decoder_bb_0, 0))
        self.connect((self.fec_extended_decoder_0_0, 0), (self.digital_diff_decoder_bb_0_0, 0))
        self.connect((self.freq_xlating_fir_filter_xxx_0, 0), (self.satellites_rms_agc_0, 0))
        self.connect((self.satellites_descrambler308_0, 0), (self.satellites_sync_to_pdu_packed_0, 0))
        self.connect((self.satellites_descrambler308_0_0, 0), (self.satellites_sync_to_pdu_packed_0_0, 0))
        self.connect((self.satellites_rms_agc_0, 0), (self.digital_costas_loop_cc_0, 0))


    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_lowpass_taps(firdes.low_pass(1.0, self.samp_rate, 4*100e3, 4*10e3, window.WIN_HAMMING, 6.76))
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)

    def get_samp_per_sym(self):
        return self.samp_per_sym

    def set_samp_per_sym(self, samp_per_sym):
        self.samp_per_sym = samp_per_sym
        self.set_rrc_taps(firdes.root_raised_cosine(self.nfilts, self.nfilts, 1.0/float(self.samp_per_sym), 0.35, 11*self.samp_per_sym*self.nfilts))

    def get_nfilts(self):
        return self.nfilts

    def set_nfilts(self, nfilts):
        self.nfilts = nfilts
        self.set_rrc_taps(firdes.root_raised_cosine(self.nfilts, self.nfilts, 1.0/float(self.samp_per_sym), 0.35, 11*self.samp_per_sym*self.nfilts))

    def get_variable_1(self):
        return self.variable_1

    def set_variable_1(self, variable_1):
        self.variable_1 = variable_1

    def get_syncword(self):
        return self.syncword

    def set_syncword(self, syncword):
        self.syncword = syncword
        self.satellites_sync_to_pdu_packed_0.set_sync(self.syncword)
        self.satellites_sync_to_pdu_packed_0_0.set_sync(self.syncword)

    def get_rrc_taps(self):
        return self.rrc_taps

    def set_rrc_taps(self, rrc_taps):
        self.rrc_taps = rrc_taps
        self.digital_pfb_clock_sync_xxx_0.update_taps(self.rrc_taps)

    def get_packet_len(self):
        return self.packet_len

    def set_packet_len(self, packet_len):
        self.packet_len = packet_len
        self.satellites_sync_to_pdu_packed_0.set_packlen(self.packet_len)
        self.satellites_sync_to_pdu_packed_0_0.set_packlen(self.packet_len)

    def get_lowpass_taps(self):
        return self.lowpass_taps

    def set_lowpass_taps(self, lowpass_taps):
        self.lowpass_taps = lowpass_taps
        self.freq_xlating_fir_filter_xxx_0.set_taps(self.lowpass_taps)

    def get_dec_cc_0(self):
        return self.dec_cc_0

    def set_dec_cc_0(self, dec_cc_0):
        self.dec_cc_0 = dec_cc_0

    def get_dec_cc(self):
        return self.dec_cc

    def set_dec_cc(self, dec_cc):
        self.dec_cc = dec_cc




def main(top_block_cls=k2sat_correct, options=None):
    tb = top_block_cls()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    tb.wait()


if __name__ == '__main__':
    main()
